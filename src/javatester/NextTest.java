/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatester;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author dennytriana
 */
public class NextTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            URL url = new URL("https://booking.airasia.com/Select.aspx?Origin1=COK&Destination1=KUL&Day1=12&Day2=19&MonthYear1=2014-01&MonthYear2=2014-01&noADT=1&noCHD=0&noINF=0&autoredirect=1&culture=en-GB&icid=fr:btn3:inen:COKKUL:rr3600104b");
            URLConnection con = url.openConnection();
            Thread.sleep(10000);
            Pattern p = Pattern.compile("text/html;\\s+charset=([^\\s]+)\\s*");
            Matcher m = p.matcher(con.getContentType());
            /* If Content-Type doesn't match this pre-conception, choose default and 
             * hope for the best. */
            String charset = m.matches() ? m.group(1) : "ISO-8859-1";
            Reader r = new InputStreamReader(con.getInputStream(), charset);
            StringBuilder buf = new StringBuilder();
            while (true) {
                int ch = r.read();
                if (ch < 0) {
                    break;
                }
                buf.append((char) ch);
            }
            String str = buf.toString();
            System.out.println(str);
        } catch (Exception e) {
            System.out.println("Ada error lagi..." + e.getMessage());
        }
    }

}
