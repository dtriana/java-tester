/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatester;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author dennytriana
 */
public class JavaTester {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        JavaTester okZ = new JavaTester();
        String url = "https://booking.airasia.com/Select.aspx";
        String urlParams = "Origin1=COK&Destination1=KUL&Day1=12&Day2=19&MonthYear1=2014-01&MonthYear2=2014-01&noADT=1&noCHD=0&noINF=0&autoredirect=1&culture=en-GB&icid=fr:btn3:inen:COKKUL:rr3600104b";
//        okZ.pingAndReportEachWhenKnown(url);
        System.out.println(JavaTester.excutePost(url, urlParams));
//        
    }

    public static String excutePost(String targetURL, String urlParameters) {
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", ""
                    + Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            //Get Response    
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {

            e.printStackTrace();
            return null;

        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public static void seleniumPost(String[] parameters) {
        HashMap params = new HashMap();
        for (String s : parameters) {
            String[] param = s.split("=");
            params.put(param[0], param[1]);
        }

        String origin = params.get("org").toString();
        String destination = params.get("dest").toString();
        String departDay = params.get("dday").toString();
        String returnDay = params.get("rday").toString();
        String mYearDepart = params.get("myd").toString();
        String mYearReturn = params.get("myr").toString();
        String noAdt = params.get("noadt").toString();
        String noInf = params.get("noinf").toString();
        String noChd = params.get("nochd").toString();
        String culture = params.containsKey("culture")
                ? "en-GB" : params.get("culture").toString();

        String url = "https://booking.airasia.com/Select.aspx?Origin1=" + origin
                + "&Destination1=" + destination + "&Day1=" + departDay + "&Day2="
                + returnDay + "&MonthYear1=" + mYearDepart + "&MonthYear2=" + mYearReturn
                + "&noADT=" + noAdt + "&noCHD=" + noChd + "&noINF=" + noInf + "&autoredirect=1&culture=" + culture;

        System.out.println("loading...." + url);
        JavaTester jt = new JavaTester();
        WebDriver driver = new FirefoxDriver();
        driver.get(url);
        if (jt.waitForPageFullyLoaded(driver, 10000)) {
            List<WebElement> headers = driver.findElements(By.cssSelector(".availabilityInputContent"));
            Iterator<WebElement> iHeaders = headers.iterator();
            while (iHeaders.hasNext()) {
                WebElement content = iHeaders.next();
                System.out.println(content.getText());
                System.out.println("ini pembatas antar header...");
            }

            List<WebElement> contents = driver.findElements(By.className("rgRow"));
            Iterator<WebElement> iContents = contents.iterator();
            while (iContents.hasNext()) {
                WebElement content = iContents.next();
                System.out.println(content.getText());
                System.out.println("ini pembatas antar row...");
            }
        }
    }

    public boolean waitForPageFullyLoaded(WebDriver driver, int timeoutMs) {
        try {
            int previous;
            int current = 0;
            int timeSliceMs = 1000;
            do {
                previous = current;
                timeoutMs -= timeSliceMs;
                current = driver.findElements(By.xpath("//*")).size();
            } while (current > previous && timeoutMs > 0);

            return timeoutMs > 0;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public static void oldSolve(String[] args) {
        try {
            URL url = new URL("https://booking.airasia.com/Select.aspx?Origin1=COK&Destination1=KUL&Day1=12&Day2=19&MonthYear1=2014-01&MonthYear2=2014-01&noADT=1&noCHD=0&noINF=0&autoredirect=1&culture=en-GB&icid=fr:btn3:inen:COKKUL:rr3600104b");
            URLConnection con = url.openConnection();
            Thread.sleep(10000);
            Pattern p = Pattern.compile("text/html;\\s+charset=([^\\s]+)\\s*");
            Matcher m = p.matcher(con.getContentType());
            /* If Content-Type doesn't match this pre-conception, choose default and 
             * hope for the best. */
            String charset = m.matches() ? m.group(1) : "ISO-8859-1";
            url.openStream();
            Reader r = new InputStreamReader(con.getInputStream(), charset);
            StringBuilder buf = new StringBuilder();
            while (true) {
                int ch = r.read();
                if (ch < 0) {
                    break;
                }
                buf.append((char) ch);
            }
            String str = buf.toString();
//            System.out.println(str);

            Reader r2 = new InputStreamReader(url.openStream(), "UTF-8");
            StringBuilder buf2 = new StringBuilder();
            while (true) {
                int ch = r2.read();
                if (ch < 0) {
                    break;
                }
                buf2.append((char) ch);
            }
            String str2 = buf.toString();
            System.out.println(str2);

        } catch (Exception e) {
            System.out.println("Ada error lagi..." + e.getMessage());
        }
    }

    public static void nextSolver() throws IOException {
        URL url = new URL("https://booking.airasia.com/Select.aspx?Origin1=COK&Destination1=KUL&Day1=12&Day2=19&MonthYear1=2014-01&MonthYear2=2014-01&noADT=1&noCHD=0&noINF=0&autoredirect=1&culture=en-GB&icid=fr:btn3:inen:COKKUL:rr3600104b");
        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
        Future future = null;

        httpCon.setDoOutput(true);
        httpCon.setRequestMethod("GET");
        OutputStreamWriter out = new OutputStreamWriter(
                httpCon.getOutputStream());
        System.out.println(httpCon.getResponseCode());
        System.out.println(httpCon.getResponseMessage());

        Reader r = new InputStreamReader(httpCon.getInputStream(), "ISO-8859-1");
        StringBuilder buf = new StringBuilder();
        while (true) {
            int ch = r.read();
            if (ch < 0) {
                break;
            }
            buf.append((char) ch);
        }
        String str = buf.toString();
        System.out.println(str);

        out.close();
    }

    public static void thirdSolver() throws Exception {

        String origin = "CGK";//params.get("org").toString();
        String destination = "DPS";//params.get("dest").toString();
        String departDay = "12";//params.get("dday").toString();
        String returnDay = "19";//params.get("rday").toString();
        String mYearDepart = "2014-12";//params.get("myd").toString();
        String mYearReturn = "2014-12";//params.get("myr").toString();
        String noAdt = "1";//params.get("noadt").toString();
        String noInf = "0";//params.get("noinf").toString();
        String noChd = "0";//params.get("nochd").toString();
        String culture = "en-GB";//params.containsKey("culture")
        //? "en-GB" : params.get("culture").toString();

        String urlParameters = "Origin1=" + origin
                + "&Destination1=" + destination + "&Day1=" + departDay + "&Day2="
                + returnDay + "&MonthYear1=" + mYearDepart + "&MonthYear2=" + mYearReturn
                + "&noADT=" + noAdt + "&noCHD=" + noChd + "&noINF=" + noInf + "&autoredirect=1&culture=" + culture;

        String encodedurl = java.net.URLEncoder.encode(urlParameters);

        URL url = new URL("https://booking.airasia.com/Select.aspx");

        HttpURLConnection connection = (java.net.HttpURLConnection) url
                .openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        ////connection.setRequestProperty("Content-Type","text/xml; charset=utf-8");

        connection.setRequestProperty("Content-Length", ""
                + Integer.toString(encodedurl.getBytes().length));
        connection.setRequestProperty("Content-Language", "en-US");

        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);

        //Send request
        DataOutputStream wr = new DataOutputStream(
                connection.getOutputStream());
        wr.writeBytes(encodedurl);
        wr.flush();
        wr.close();
        System.out.println(connection.getResponseCode());

        //Get Response
        InputStream is = connection.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        StringBuffer responseBuffer = new StringBuffer();
        while ((line = rd.readLine()) != null) {
            responseBuffer.append(line);
            responseBuffer.append('\r');
        }
        rd.close();
        System.out.println("\n\nRESPONSE\n\n" + responseBuffer.toString());
        System.out.println(connection.getErrorStream());
    }

    public void pingAndReportEachWhenKnown(String url) throws InterruptedException, ExecutionException, IOException {

        ExecutorService executor = Executors.newFixedThreadPool(1);
        CompletionService<PingResult> compService = new ExecutorCompletionService<>(executor);

        Task task = new Task(url);
        compService.submit(task);

        Future<PingResult> future = compService.take();
        PingResult res1 = future.get();

        InputStream is = res1.getInputStream();
        System.out.println("Output 1 ==== " + this.processOutput(is));

        InputStream is2 = res1.getOpenedStream();
        System.out.println("Output 2 ==== " + this.processOutput(is2));
        executor.shutdown(); //always reclaim resources
    }

    /**
     * Simple struct to hold all the date related to a ping.
     */
    private static final class PingResult {

        String URL;
        Boolean SUCCESS;
        Long TIMING;
        InputStream STREAM, STREAM2;

        @Override
        public String toString() {
            return "Result:" + SUCCESS + " " + TIMING + " msecs " + URL;
        }

        public InputStream getInputStream() {
            return STREAM;
        }

        public InputStream getOpenedStream() {
            return STREAM2;
        }
    }

    /**
     * Try to ping a URL. Return true only if successful.
     */
    private final class Task implements Callable<PingResult> {

        Task(String aURL) {
            fURL = aURL;
        }

        /**
         * Access a URL, and see if you get a healthy response.
         */
        @Override
        public PingResult call() throws Exception {
            return pingAndReportStatus(fURL);
        }
        private final String fURL;
    }

    private PingResult pingAndReportStatus(String aURL) throws MalformedURLException {
        PingResult result = new PingResult();
        result.URL = aURL;
        long start = System.currentTimeMillis();
        URL url = new URL(aURL);

        try {
            result.STREAM = url.openStream();
            URLConnection connection = url.openConnection();
            int FIRST_LINE = 0;
            String firstLine = connection.getHeaderField(FIRST_LINE);
            result.SUCCESS = true;
            long end = System.currentTimeMillis();
            result.TIMING = end - start;
            result.STREAM2 = connection.getInputStream();
        } catch (IOException ex) {
            //ignore - fails
        }
        return result;
    }

    private String processOutput(InputStream ip) throws IOException {

        StringBuffer responseBuffer;

        try (BufferedReader rd = new BufferedReader(new InputStreamReader(ip))) {
            String line;
            responseBuffer = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                responseBuffer.append(line);
                responseBuffer.append('\r');
            }
        }

        return "\n\nRESPONSE\n\n" + responseBuffer.toString();
    }

}
